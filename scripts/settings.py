import MySQLdb
import os

def ConnectionMysql(charset = "utf8"):
    conn_mysql = MySQLdb.connect(host = "127.0.0.1",user = "root",passwd="",db="investmentsdb", charset = charset)
    return conn_mysql

DATA_PATH = os.path.dirname(os.path.realpath(__file__))
BASE_URL = ""