#!/usr/bin/python
from settings import ConnectionMysql
from crawling_lib import *
import inspect
from settings import DATA_PATH
from lxml import etree
from datetime import datetime

logging_file_path = inspect.getfile(inspect.currentframe()).rstrip(".py") + ".log"
# logging = custom_logging(data_path=DATA_PATH, logfile=logging_file_path)
logging = custom_logging(data_path=DATA_PATH, logfile=None)

connection = ConnectionMysql()
cursor = connection.cursor()
base_url = ""
alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
for i,a in enumerate(alphabets):
    # if i < 13:
    #     continue
    u = "http://www.moneycontrol.com/india/mutualfunds/mutualfundsinfo/snapshot/%s" % (a)
    print(u)
    root = open_url(u, logging)
    if root is None:
        logging.error("Could Not open base url.")
        continue
    raw_html = (etree.tostring(root, pretty_print=True)).encode('utf-8')
    cursor.execute("""INSERT INTO  raw_htmls (url, raw_html, crawl_date) values (%s, %s, %s) """ ,(u, raw_html, datetime.now()))
    rawhtml_id = cursor.lastrowid
    scripts = root.xpath("//a[@class='verdana12blue']")
    for s in scripts:
        try:
            href = utf8_to_ascii(s.xpath("@href")[0])
        except Exception as e:
            print(e)
        try:
            mc_script_id = utf8_to_ascii(href.rsplit("/",1)[1])
        except Exception as e:
            print(e)
        try:
            name = utf8_to_ascii(printstr(s))
        except Exception as e:
            print(e)

        # print(raw_html)
        # sql = """INSERT INTO  1mutual_funds (name, mc_script_id, href, date_crawled, raw_html) values ('%s', '%s', '%s', '%s', '%s') """ %(name, mc_script_id, href, datetime.now(), raw_html)
        print(href, name)
        try:
            cursor.execute("""INSERT INTO  1mutual_funds (name, mc_script_id, url, date_crawled, raw_html_id) values (%s, %s, %s, %s, %s) """ ,(name, mc_script_id, href, datetime.now(), rawhtml_id))
            connection.commit()
        except Exception as e:
            print(e)

            # len(scripts)
    # break

