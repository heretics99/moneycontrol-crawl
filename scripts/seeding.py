#!/usr/bin/python
from settings import ConnectionMysql
from crawling_lib import *
import inspect
from settings import DATA_PATH
from datetime import datetime

logging = custom_logging(data_path=DATA_PATH, logfile=inspect.getfile(inspect.currentframe()).rstrip(".py") + ".log")

connection = ConnectionMysql()
cursor = connection.cursor()
base_url = ""
cursor.execute ("SELECT id, mc_id from moneycontrol_scripts where type=2")
while(1):
    row = cursor.fetchone()
    if row == None:
        break
    u = "http://www.moneycontrol.com/mf/mf_info/mf_graph.php?im_id=%s" % (row[1])
    print(u)
    root = open_url(u, logging)
    if root is None:
        logging.error("Could Not open base url.")
        continue
    data = (printstr_preservenewline(root)).split("\n")
    for line in data:
        line = line.split(",")
        d = datetime.strptime(line[0], "%d %b %Y")
        sql = """INSERT INTO  history (script_id, value, date_recorded) values ('%s','%s','%s') """ %(row[0], line[1], d)
        cursor.execute(sql)
        connection.commit()
# try:
#         url = sanitize(row.find('h3/a').attrib['href'])
#         name = rchop(sanitize(row.find('h3/a').attrib['title']), "Cars").strip()
#         logging.info(url)
#         sql = """INSERT INTO """+TABLE_NAME1+"""(name , url) values ('%s','%s') """ %(name,url)

#     except Exception as e:
#         logging.error("%s"%(e))
