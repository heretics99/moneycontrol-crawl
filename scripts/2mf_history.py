#!/usr/bin/python
from settings import ConnectionMysql
from crawling_lib import *
import inspect
from settings import DATA_PATH
from datetime import datetime
from Queue import Queue
from threading import Thread, Lock

logging = custom_logging(data_path=DATA_PATH, logfile=inspect.getfile(inspect.currentframe()).rstrip(".py") + ".log")

connection = ConnectionMysql()
cursor = connection.cursor()
lock = Lock()
queue = Queue()
max_threads = 30


class ThreadedCrawling(Thread):
    """Crawling Class"""
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            row = self.queue.get() #getting object from queue
            crawlHistoryData(row)
            self.queue.task_done()


def startthread():
    for row in getMFHistoryToBeCrawled():
        queue.put(row)

    #spawning threads
    for i in range(max_threads):
        t = ThreadedCrawling(queue)
        t.setDaemon(True)
        t.start()
    queue.join() #wait till entire queue is processed


def getMFHistoryToBeCrawled():
    cursor.execute ("SELECT id, mc_script_id from 1mutual_funds where date_history_crawled <= DATE_SUB(NOW(), INTERVAL 1 MINUTE) order by id asc")
    rows = cursor.fetchall()
    return rows


def crawlHistoryData(row):
    if row == None:
        return
    mc_db_id = row[0]
    mc_script_id = row[1]
    u = "http://www.moneycontrol.com/mf/mf_info/mf_graph.php?im_id=%s" % (mc_script_id)
    root = open_url(u, logging)
    if root is None:
        logging.error("Could Not open base url for SCRIPT %s"%(mc_script_id))
        return
    data = (printstr_preservenewline(root)).split("\n")
    sql = "SELECT MAX(date_recorded) from 2historic_data where mc_db_id = '%s'"%(mc_db_id)
    with lock:
        cursor.execute(sql)
        maxdate = cursor.fetchone()[0]

    # with lock:
    count = 0
    for line in data:
        line = line.split(",")
        d = datetime.strptime(line[0], "%d %b %Y")
        v1 = line[1]
        v2 = line[2]
        v3 = line[3]
        v4 = line[4]
        if maxdate is None or d>maxdate:
            sql = """INSERT INTO 2historic_data (mc_db_id, mc_script_id, v1, v2, v3, v4, date_recorded, date_crawled) values ('%s','%s','%s','%s','%s','%s','%s', '%s') """ %(mc_db_id, mc_script_id, v1, v2, v3, v4, d, datetime.now())
            with lock:
                cursor.execute(sql)
                count += 1
    # if count>0:
    sql = """UPDATE 1mutual_funds SET date_history_crawled = '%s' where id = '%s'""" %(datetime.now(), mc_db_id)
    with lock:
        cursor.execute(sql)
        connection.commit()
        logging.info("%s - UPDATED values for %s days"%(mc_script_id, count))

# for row in getMFHistoryToBeCrawled():
#     crawlHistoryData(row)
startthread()
