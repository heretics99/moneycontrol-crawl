#!/usr/bin/env python

"""
Python 2.7
A library of common functions needed in data crawling.

**************************
USAGE:
**************************
import inspect, os
from common_functions import *
logging = custom_logging(logfile = inspect.getfile(inspect.currentframe()).rstrip(".py")+".log")
root = open_url(URL, logging)

Detailed Documentation of open_url() function at the end.

* This module's code is pep8 compliant. Except for error E501 "line too long (82 characters)"
* Make sure future versions are pep8 compliant too. To verify compliance, use the command:
pep8 --ignore=E501 --show-source common_functions.py
"""

import sys
import os
import errno
import datetime
import select
# import tty
# import termios
from lxml.html import parse
import urllib
import urllib2
import httplib
import time
import logging
import socket
import re
from xml.sax.saxutils import escape as xml_escape
from xml.sax.saxutils import unescape as xml_unescape


__author__ = "Prakhar Kumar Goel"
__version__ = "1.0"
__python_version__ = "2.7"
__email__ = "prakhar@prakhargoel.com"
__status__ = "Beta"

DEFAULT_PROXIES = [
    'http://proxyserver9911.appspot.com/',
    'http://proxyserver1101.appspot.com/',
    'http://mirrorrr.appspot.com/',
    'http://labnol-proxy-server.appspot.com/',
]

_HEADERS = {
    "User-Agent": "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"
}


def mkdir_p(path):
    """
    Creates the path, recursively creating all directories in this path.
    """
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise


def timeStamped(fname, fmt='{fname}_%Y-%m-%d_%H-%M-%S.{ext}'):
    """
    Given a file name, appends timestamp just before its extension.
    Filename is compulsory. time format is optional.
    """
    tmp = fname.rsplit(".", 1)
    fnameonly = tmp[0]
    ext = tmp[1]
    return datetime.datetime.now().strftime(fmt).format(fname=fnameonly,
                                                        ext=ext)


def custom_logging(data_path, dir_name="logs",
                   name="logs", logfile="logs.log"):
    """
    Creates and returns a custom logger object. This custom logger should
    be used for all logging purposes.
    Compulsory Arguments:
        data_path -- Path to directory where log folder will be created and
                     log file will be saved.
    Optional Arguments:
        dir_name -- Directory which will reside in data_path, containing actual
                    log files. Default: "logs"
        name -- name of logger object. default: "logs"
        logfile -- log filename. Default: "logs.log"
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    handler_console = logging.StreamHandler()
    formatter_console = logging.Formatter('[%(levelname)s] %(message)s')
    handler_console.setFormatter(formatter_console)
    logger.addHandler(handler_console)
    if logfile:
        full_qualified_dir = os.path.join(data_path, dir_name)
        mkdir_p(full_qualified_dir)
        logfile_full = os.path.join(full_qualified_dir, timeStamped(logfile))
        handler_file = logging.FileHandler(logfile_full)
        formatter_file = logging.Formatter(
            '%(asctime)s [%(name)s][%(levelname)s] %(message)s')
        handler_file.setFormatter(formatter_file)
        logger.addHandler(handler_file)
    return logger


def sanitize(value):
    """
    Given value, removes multiple consecutive spaces and replace it
    with 1 space. Also strips junk characters.
    """
    value = re.sub(' +', ' ', value)
    value = value.strip(": /={}")
    return (value)


def utf8_to_ascii(text, replacement=" "):
    """
    Removes all utf8 characters and replaces it with a single space by default
    """
    return ''.join([i if ord(i) < 128 else replacement for i in text])

def printstr_preservenewline(node_element):
    """
    Returns string under a node. This also includes string
    from children nodes, concatenated together.
    Replaces multiple consecutive spaces by a single space.
    """
    if node_element is not None:
        return re.sub(' +', ' ', utf8_to_ascii(node_element.xpath('string()').strip()))
    else:
        return ''


def printstr(node_element):
    """
    Same as printstr_preservenewline. But replaces multilines and tabs by single space.
    """
    if node_element is not None:
        return printstr_preservenewline(node_element).replace("\n", " ").replace("\r", " ").replace("\t", " ").strip()
    else:
        return ''


def printstr_nodelevel(node_element):
    """
    Returns string directly under a node. Does NOT include string from children nodes.
    """
    if node_element is not None:
        return re.sub(' +', ' ', node_element.text.replace("\n", " ").replace("\r", " ").replace("\t", " ")).strip()
    else:
        return ''


def lchop(str, wrd):
    """
    Removes wrd if it appears at the start of a str.
    """
    l = len(wrd)
    if str[:l] == wrd:
        return str[l:]
    return str


def rchop(str, wrd):
    """
    Removes wrd if it appears at the end of str.
    """
    l = len(wrd)
    if str[-l:] == wrd:
        return str[:-l]
    return str

#HTML escaping
html_escape_table = {
    '"': "&quot;",
    "'": "&apos;"
}


def escape(text):
    """
    escapes HTML string. By default Converts:
    < to &lt;
    > to &gt;
    & to &amp;
    For more characters, pass a dictionary of characters and their escape values.
    """
    return xml_escape(text, html_escape_table)


def unescape(text):
    """
    unescapes HTML string. By default Converts:
    &lt to <;
    &gt to >;
    &amp to &;
    For more characters, pass a dictionary of escape values and corresponding characters.
    """
    return xml_unescape(text, dict((v, k) for k, v in html_escape_table.iteritems()))


def string_to_float(st):
    """
    Returns the first decimal number from a string.
    """
    try:
        return float(re.findall("[-+]?\d*\.\d+|\d+", str(st).replace(",", ""))[0])
    except Exception as e:
        return 0.0


def is_number(s):
    """
    Checks if given string is number. Valid examples:
    '100' -- True
    '98.68' -- True
    '2.7845e+07' -- True
    '8.7845e-07' -- True
    'abc' -- False
    """
    try:
        float(s)
        return True
    except ValueError:
        return False


def calc_time(traw):
    """
    Calculates time notation into number of seconds. Example:
    '7.7s' -- 7.7
    '5.54m' -- 332.4
    '.8h' -- 2880
    """
    tfactor = {
        's': 1,
        'm': 60,
        'h': 3600,
    }
    if is_number(traw[:-1]):
        return float(traw[:-1]) * tfactor.get(traw[-1])
    else:
        return None


def isData():
    """ Checks if user input is received on console."""
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])


def check_user_input(old_settings, logging):
    """
    Pause/Resume process if user input is received.
    Allows deferred processing. For eg: '5m' means defer processing
        for 5 mins.
    """
    if isData():
        c = sys.stdin.read(1)
        if c:
            if c == 'p':
                termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
                g = '\x1b'
                g = raw_input("""
Paused. Use the Following commands now:
Hit 'n' to skip and continue with next link.
Hit '5s' or '3m' or '2h' to wait for 5 secs, 3 mins or 2 hours
Hit Enter or Escape to continue from here itself. \nEnter choice:""")
                tty.setcbreak(sys.stdin.fileno())
            if g == '\x1b':
                logging.info("Continuing...")
            elif g == 'n':
                #log error
                return False
            elif len(g) > 0 and g[-1] in ['s', 'm', 'h']:
                tval = calc_time(g)
                if tval is not None:
                    logging.info("Waiting for %s seconds." % (tval))
                    time.sleep(tval)


class HeadRequest(urllib2.Request):
    """ Internal function. Used in getheadersonly."""
    def get_method(self):
        return 'HEAD'


def getheadersonly(url, redirections=True):
    """
    Used for getting only the header information for a url, without its body (body is not even downloaded).
    Returns a dictionary containing response code, headers and final url.
    Redirections are enabled by default, but can be stopped.
    """
    opener = urllib2.OpenerDirector()
    opener.add_handler(urllib2.HTTPHandler())
    opener.add_handler(urllib2.HTTPDefaultErrorHandler())
    if redirections:
        # HTTPErrorProcessor makes HTTPRedirectHandler work
        opener.add_handler(urllib2.HTTPErrorProcessor())
        opener.add_handler(urllib2.HTTPRedirectHandler())
    try:
        res = opener.open(HeadRequest(url))
    except urllib2.HTTPError, res:
        pass
    res.close()
    return dict(code=res.code, headers=res.info(), finalurl=res.geturl())


def open_url(url, logging,
             data_dict={},
             headers=_HEADERS,
             max_retrys=50,
             max_retrys_original_url=2,
             sleep_time=2,
             sockettimeout=20,
             proxies=DEFAULT_PROXIES,
             return_moreinfo=False,
             redirections=True,
             isfilepath=False):
    """
    Main and most used function in crawling. Accepts a url and logger object as compulsory arguments.
    * returns xpath root object for the crawled page. The client need not import any associated libraries.
    * Automatically retries link on network failures.
    * Can Pause and Play crawling.
    * Switch to proxy servers in case of IP blockings.

    Optional arguments:
        data_dict -- dict of GET arguments, which are appended to url.
                     (POST arguments not supported yet)
        headers -- Header to send while opening link.
                    Default is "User-Agent": "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)"
                    Custom headers can be sent.
        max_retrys -- Maximum retries for a url.
        sleep_time -- sleep timer between retries in seconds
        sockettimeout -- socket timeout in seconds
        proxies -- list of proxy servers to use in case of retries.
                    They are tried one by one, repeating from start unless url is accessed.
        return_moreinfo -- Flag, False by default. If set True, returns a tuple of
                            * xpath root object, and
                            * moreinfo about the url (headers and final url)
        redirections -- Flag, True by default. Follows redirections if set True.
        isfilepath -- Flag, False by default. If set True, assumes 'url' is a local HTML file path,
                        instead of a http url.
        **Note: The pause and resume feature only works when network connection is down.
                To pause crawling, press 'p'
                To defer crawling, type '8m' (defer by 8 mins), '.5h' (30 mins), '50s'(50 secs)
    """
    socket.setdefaulttimeout(sockettimeout)
    # old_settings = termios.tcgetattr(sys.stdin)
    moreinfo = {}
    if data_dict == {}:
        data = None
    else:
        data = urllib.urlencode(data_dict)
        proxies = []
    try:
        # tty.setcbreak(sys.stdin.fileno())
        retried = 0
        while True:
            # if check_user_input(old_settings, logging) is False:
            #     if not return_moreinfo:
            #         return None
            #     else:
            #         return None, moreinfo
            try:
                localurl = url
                if not isfilepath:
                    delta = retried % ((len(proxies)) + max_retrys_original_url)
                    if delta <= max_retrys_original_url-1:
                        localurl = url
                    else:
                        localurl = str(proxies[delta-max_retrys_original_url]) + url.replace("http://", "")
                    if retried > 0:
                        logging.info("Requesting from url: %s . Data= '%s'. retried=%s, delta=%s" % (localurl, data, retried, delta))
                    localurl = localurl.replace(" ", "+")
                    req = urllib2.Request(localurl, data=data, headers=headers)
                    con = urllib2.urlopen(req)
                    moreinfo['headers'] = dict(con.info())
                    moreinfo['finalurl'] = con.geturl()
                else:
                    con = open(localurl, 'r')
                    moreinfo['headers'] = ''
                    moreinfo['finalurl'] = url
                doc = parse(con)
                root = doc.getroot()
                if not return_moreinfo:
                    return root
                else:
                    return root, moreinfo
            except Exception as e:
                retried += 1
                import os
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                logging.error("Error occurred:%s. In %s on Line No:%s. URL: %s" % (e, fname, exc_tb.tb_lineno, localurl))
                if retried <= max_retrys:
                    logging.info("""Retrying %sth time after %s seconds... (Change your ip if problem persists)""" %
                                 (retried, sleep_time))
                    time.sleep(sleep_time)
                    continue
                else:
                    logging.warning("Max Retries reached. Quitting this lap.")
            break
    finally:
        # termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
        pass
    if not return_moreinfo:
        return None
    else:
        return None, moreinfo