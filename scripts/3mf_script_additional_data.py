from settings import ConnectionMysql
from crawling_lib import *
import inspect
from settings import DATA_PATH
from Queue import Queue
from threading import Thread, Lock
from lxml import etree
from collections import OrderedDict
from datetime import datetime
import traceback

logging_file_path = inspect.getfile(inspect.currentframe()).rstrip(".py") + ".log"
logging = custom_logging(data_path=DATA_PATH, logfile=logging_file_path)
connection = ConnectionMysql()
cursor = connection.cursor()
lock = Lock()
queue = Queue()
max_threads = 30

class ThreadedCrawling(Thread):
    """Crawling Class"""
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            row = self.queue.get() #getting object from queue
            crawlData(row)
            self.queue.task_done()



def startthread():
    for row in getMFsToBeCrawled():
        queue.put(row)

    #spawning threads
    for i in range(max_threads):
        t = ThreadedCrawling(queue)
        t.setDaemon(True)
        t.start()

    queue.join() #wait till entire queue is processed

def getMFsToBeCrawled():
    cursor.execute ("SELECT id, mc_script_id, url from 1mutual_funds where date_crawled <= DATE_SUB(NOW(), INTERVAL 10 MINUTE ) order by id asc")
    rows = cursor.fetchall()
    return rows


def crawlData(row):
    mc_db_id = row[0]
    mc_script_id = row[1]
    url = row[2]
    # url = '/mutual-funds/nav/baroda-pioneer-balance-fund/MHD005'
    if not url:
        return
    baseurl = "http://moneycontrol.com"
    url = "%s%s"%(baseurl, url)
    root = open_url(url, logging)
    if root is None:
        logging.error("Could Not open url '%s'"%url)
        return

    values = OrderedDict()
    try:
        values['fund_category'] = root.xpath("//a[@class='bl_14']/@titlt")[0]
    except Exception as e:
        logging.error("%s -- 'fund_category' -- %s"%(e, url))

    try:
        values['crisil_rating'] = printstr(root.xpath("//a[@href='/mf/crisil_methodology/#rank_def']")[0])
    except Exception as e:
        logging.error("%s -- 'crisil_rating' -- %s"%(e, url))

    try:
        values['riskometer'] = printstr(root.xpath("//span[@class='FL UC']")[0])
    except Exception as e:
        logging.error("%s -- 'riskometer' -- %s"%(e, url))

    try:
        values['investment_objective'] = printstr(root.xpath("//p[@class='b_13 PT5']")[0])
    except Exception as e:
        logging.error("%s -- 'investment_objective:%s' -- %s"%(e, root.xpath("//p[@class='b_13 PT5']")[0], url))

    try:
        values['fund_type'] = printstr(root.xpath("//td[../th/text()='Fund Type']")[0])
    except Exception as e:
        logging.error("%s -- 'fund_type' -- %s"%(e, url))

    try:
        values['investment_plan'] = printstr(root.xpath("//td[../th/text()='Investment Plan']")[0])
    except Exception as e:
        logging.error("%s -- 'investment_plan' -- %s"%(e, url))

    try:
        values['launch_date'] = printstr(root.xpath("//td[../th/text()='Launch date']")[0])
    except Exception as e:
        logging.error("%s -- 'launch_date' -- %s"%(e, url))

    try:
        values['benchmark'] = printstr(root.xpath("//td[../th/text()='Benchmark']")[0])
    except Exception as e:
        logging.error("%s -- 'benchmark' -- %s"%(e, url))

    try:
        values['asset_size_in_cr'] = printstr(root.xpath("//td[../th/text()='Asset Size (Rs cr)']")[0])
    except Exception as e:
        logging.error("%s -- 'asset_size_in_cr' -- %s"%(e, url))

    try:
        values['minimum_investment'] = printstr(root.xpath("//td[../th/text()='Minimum Investment']")[0])
    except Exception as e:
        logging.error("%s -- 'minimum_investment' -- %s"%(e, url))

    try:
        values['last_dividend'] = printstr(root.xpath("//td[../th/text()='Last Dividend']")[0])
    except Exception as e:
        logging.error("%s -- 'last_dividend' -- %s"%(e, url))

    try:
        values['bonus'] = printstr(root.xpath("//td[../th/text()='Bonus']")[0])
    except Exception as e:
        logging.error("%s -- 'bonus' -- %s"%(e, url))

    try:
        values['fund_manager'] = "%s,%s"%(root.xpath("//td[../th/text()='Fund Manager']/text()")[0], root.xpath("//td[../th/text()='Fund Manager']/a/@href")[0])
    except Exception as e:
        logging.error("%s -- 'fund_manager' -- %s"%(e, url))

    try:
        values['notes'] = printstr(root.xpath("//td[../th/text()='Notes']")[0])
    except Exception as e:
        logging.error("%s -- 'notes' -- %s"%(e, url))

    try:
        values['entry_load'] = printstr(root.xpath("//td[../th/text()='Entry Load']")[0])
    except Exception as e:
        logging.error("%s -- 'entry_load' -- %s"%(e, url))

    try:
        values['exit_load'] = printstr(root.xpath("//td[../th/text()='Exit Load']")[0])
    except Exception as e:
        logging.error("%s -- 'exit_load' -- %s"%(e, url))

    try:
        values['load_comments'] = printstr(root.xpath("//td[../th/text()='Load Comments']")[0])
    except Exception as e:
        logging.error("%s -- 'load_comments' -- %s"%(e, url))

    try:
        values['regd_office'] = printstr(root.xpath("//td[../th/text()='Regd. Office']")[0])
    except Exception as e:
        logging.error("%s -- 'regd_office' -- %s"%(e, url))
        # traceback.print_exc()

    try:
        values['tel_no'] = printstr(root.xpath("//td[../th/text()='Tel. No.']")[0])
    except Exception as e:
        logging.error("%s -- 'tel_no' -- %s"%(e, url))

    try:
        values['fax_no'] = printstr(root.xpath("//td[../th/text()='Fax No.']")[0])
    except Exception as e:
        logging.error("%s -- 'fax_no' -- %s"%(e, url))

    try:
        values['email'] = printstr(root.xpath("//td[../th/text()='Email']")[0])
    except Exception as e:
        logging.error("%s -- 'email' -- %s"%(e, url))

    try:
        values['website'] = printstr(root.xpath("//td[../th/text()='Website']")[0])
    except Exception as e:
        logging.error("%s -- 'website' -- %s"%(e, url))


    for k, v in values.items():
        values[k] = utf8_to_ascii(v)

    sql = "UPDATE 1mutual_funds SET {}, date_crawled='{}' where id = {}".format(', '.join('{}=%s'.format(k) for k in values), datetime.now(), mc_db_id)

    with lock:
        cursor.execute(sql, values.values())
        connection.commit()
        logging.info("%s - UPDATED"%url)

# for row in getMFsToBeCrawled():
#     crawlData(row)

startthread()
